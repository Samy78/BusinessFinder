import { Injectable } from '@angular/core';
import{HttpClient} from "@angular/common/http";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

import { Observable } from "rxjs/Rx";
@Injectable()
export class SearchService {
  categories;
  results;

  //transfer data from accueil to result
  private datasearch = new BehaviorSubject<any>(null);
  datapassing = this.datasearch.asObservable();
  changeResult(show: any) {
    this.datasearch.next(show)
  }
  //transfer boolean toolbar
  private show = new BehaviorSubject<boolean>(false);
  currentMessage = this.show.asObservable();
  changeMessage(show: boolean) {
    this.show.next(show)
  }

  constructor(private http: HttpClient) { }

  public get(): Observable<any> {

    if(this.categories != null)
    {
      console.log(this.categories);
      return this.categories ;          // Observable.of(this.categories)     problem import 'of'
    }
    else
    {

      this.categories= this.http.get(`http://localhost:8080/categories`)

      return this.categories;
    }
  }

   search(location : string, categorie:string, term: string,longitude,latitude):any {

     this.results= this.http.get(`http://localhost:8080/search?location=`+location + '&categorie='+categorie+'&term='+term +'&longitude='+longitude+'&latitude='+latitude);
     return this.results;


  }
}
