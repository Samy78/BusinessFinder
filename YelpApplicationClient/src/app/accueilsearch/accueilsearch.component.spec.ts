import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilsearchComponent } from './accueilsearch.component';

describe('AccueilsearchComponent', () => {
  let component: AccueilsearchComponent;
  let fixture: ComponentFixture<AccueilsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueilsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
